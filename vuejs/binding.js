var app = new Vue({
    el: '#app',
    data: {
        title:'Mèo con giống Nhật Bản',
        url: 'https://www.google.com/',
        target: '_blank',
        price: 10000,
        selectedProduct : 1,
        cartNumber : 0,
        comment: '',
        detail :[
            'Mèo con giống nhật bản hiền lành, nhanh nhẹn',
            'Laptop siêu rẻ, rẻ hơn hạt rẻ',
            'Pháo hoa bawdn không sáng'
        ],
        listProduct: [
            {
                img: './images/anh0.jpg',
                quantity: 2,
                textProduct: 'Laptop'
            },
            {
                img: './images/1074-5472x3648.jpg',
                quantity: 8,
                textProduct: 'Mèo con'
            },
            {
                img: './images/anh1.jpg',
                quantity: 0,
                textProduct: 'Pháo hoa'
            }
        ]
    },
    methods: {
        handleClick (e, index){
            this.selectedProduct = index;
        },
        handleAddToCart (e){
            if (this.cartNumber + 1 > this.getProduct.quantity)
            {
                alert('Không đủ hàng!');
            }else{
                this.cartNumber = this.cartNumber + 1;
            }
        }
    }, 
    computed: {
        priceFormat (){
            var number = this.price;
            return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(number);
        },
        getProduct (){
            let index = this.selectedProduct;
            return this.listProduct[index];
        }
    }
})